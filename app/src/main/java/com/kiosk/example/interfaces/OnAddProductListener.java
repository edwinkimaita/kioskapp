package com.kiosk.example.interfaces;

public interface OnAddProductListener {
    void onClick(String productName, String productDesc, float productPrice);
}
